pub use crate::{
    serial::ReactorExt as _,
    timer::ReactorExt as _,
};

#[doc(hidden)]
pub use stm32f1xx_hal::prelude::*;
