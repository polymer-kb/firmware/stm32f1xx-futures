#![no_std]
#![feature(const_fn, never_type)]
#![warn(missing_docs)]

//! Async Hardware Abstraction Layer for stm32f1xx

pub use stm32f1xx_hal::{
    self as hal,
    stm32,
};

pub mod serial;

/// Async HAL Prelude
pub mod prelude;

/// Async Timers
pub mod timer;
