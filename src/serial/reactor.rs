use core::{
    marker::PhantomData,
    pin::Pin,
};

use rtfm::Mutex;

use embrio::io::{
    Read,
    Write,
};

use embedded_hal::serial::{
    Read as _,
    Write as _,
};

use futures::task::{
    Context,
    Poll,
    Waker,
};

use futures_intrusive::buffer::{
    ArrayBuf,
    RealArray,
    RingBuf,
};

use stm32f1xx_hal::{
    serial::{
        Rx as HalRx,
        Tx as HalTx,
    },
    stm32::{
        USART1,
        USART2,
        USART3,
    },
};

/// A reactor for a serial device
///
/// Manages reading/writing to a serial port and the wakers for tasks that
/// request to read/write.
pub struct Reactor<USART, B>
where
    B: RealArray<u8> + AsRef<[u8]> + AsMut<[u8]>,
{
    read_owned: bool,
    write_owned: bool,
    device: (HalTx<USART>, HalRx<USART>),
    read_buf: ArrayBuf<u8, B>,
    write_buf: ArrayBuf<u8, B>,

    write_waker: Option<Waker>,
    flush_waker: Option<Waker>,
    read_waker: Option<Waker>,
}

impl<USART, B> Reactor<USART, B>
where
    B: RealArray<u8> + AsRef<[u8]> + AsMut<[u8]>,
{
    /// Create a new reactor for a serial port.
    pub fn new(device: (HalTx<USART>, HalRx<USART>)) -> Self {
        Reactor {
            device,
            read_owned: false,
            write_owned: false,
            read_buf: ArrayBuf::new(),
            write_buf: ArrayBuf::new(),

            read_waker: None,
            write_waker: None,
            flush_waker: None,
        }
    }

    unsafe fn steal(&mut self) -> bool {
        let preowned = self.read_owned || self.write_owned;
        self.write_owned = true;
        self.read_owned = true;
        !preowned
    }

    fn take(&mut self) {
        unsafe { assert!(self.steal()) }
    }

    unsafe fn release_write(&mut self) {
        self.write_owned = false;
    }
    unsafe fn release_read(&mut self) {
        self.read_owned = false;
    }
}

/// Asynchronous serial transmitter
pub struct Tx<R, USART, B>
where
    B: RealArray<u8> + AsRef<[u8]> + AsMut<[u8]>,
    R: Mutex<T = Reactor<USART, B>>,
{
    reactor: R,
    _ph: PhantomData<Reactor<USART, B>>,
}

impl<R, USART, B> Drop for Tx<R, USART, B>
where
    B: RealArray<u8> + AsRef<[u8]> + AsMut<[u8]>,
    R: Mutex<T = Reactor<USART, B>>,
{
    fn drop(&mut self) {
        self.reactor.lock(|reactor| unsafe {
            reactor.release_write();
        });
    }
}

/// Asynchronous serial receiver
pub struct Rx<R, USART, B>
where
    B: RealArray<u8> + AsRef<[u8]> + AsMut<[u8]>,
    R: Mutex<T = Reactor<USART, B>>,
{
    reactor: R,
    _ph: PhantomData<Reactor<USART, B>>,
}

impl<R, USART, B> Drop for Rx<R, USART, B>
where
    B: RealArray<u8> + AsRef<[u8]> + AsMut<[u8]>,
    R: Mutex<T = Reactor<USART, B>>,
{
    fn drop(&mut self) {
        self.reactor.lock(|reactor| unsafe {
            reactor.release_read();
        });
    }
}

unsafe impl<R, USART, B> Send for Rx<R, USART, B>
where
    B: RealArray<u8> + AsRef<[u8]> + AsMut<[u8]>,
    R: Mutex<T = Reactor<USART, B>> + Send,
{
}

unsafe impl<R, USART, B> Send for Tx<R, USART, B>
where
    B: RealArray<u8> + AsRef<[u8]> + AsMut<[u8]>,
    R: Mutex<T = Reactor<USART, B>> + Send,
{
}

/// Extension trait for rtfm mutexes around a serial reactor
pub trait ReactorExt<USART, B>: Mutex<T = Reactor<USART, B>> + Sized
where
    B: RealArray<u8> + AsRef<[u8]> + AsMut<[u8]>,
{
    /// "Take" this reactor's port. Panics if called while the previous `Tx` and
    /// `Rx` are still alive.
    fn take(&mut self) -> (Tx<&'_ mut Self, USART, B>, Rx<&'_ mut Self, USART, B>);
}

macro_rules! hal {
    ($usart:ident) => {
        impl<B> Reactor<$usart, B>
        where
            B: RealArray<u8> + AsRef<[u8]> + AsMut<[u8]>,
        {
            fn poll_write(&mut self, cx: &mut Context<'_>, buf: &[u8]) -> Poll<usize> {
                if buf.is_empty() {
                    panic!("cannot call write with an empty buffer");
                }
                unsafe { &*$usart::ptr() }
                    .cr1
                    .modify(|_, w| w.txeie().set_bit());
                if !self.write_buf.can_push() {
                    if let Some(ref mut waker) = self.write_waker {
                        if !cx.waker().will_wake(waker) {
                            *waker = cx.waker().clone()
                        }
                    } else {
                        self.write_waker = Some(cx.waker().clone())
                    }
                    return Poll::Pending;
                }

                let mut idx = 0;
                while self.write_buf.can_push() && idx < buf.len() {
                    self.write_buf.push(buf[idx]);
                    idx += 1;
                }

                Poll::Ready(idx)
            }

            fn poll_flush(&mut self, cx: &mut Context<'_>) -> Poll<()> {
                let sr = unsafe { &*$usart::ptr() }.sr.read();
                if self.write_buf.is_empty() && sr.tc().bit_is_set() {
                    Poll::Ready(())
                } else {
                    unsafe { &*$usart::ptr() }
                        .cr1
                        .modify(|_, w| w.tcie().set_bit());
                    unsafe { &*$usart::ptr() }
                        .cr1
                        .modify(|_, w| w.txeie().set_bit());
                    let new_waker = cx.waker();
                    if let Some(ref mut waker) = self.flush_waker {
                        if !new_waker.will_wake(waker) {
                            *waker = new_waker.clone();
                        }
                    } else {
                        self.flush_waker = Some(new_waker.clone())
                    }
                    Poll::Pending
                }
            }

            fn poll_read(&mut self, cx: &mut Context<'_>, buf: &mut [u8]) -> Poll<usize> {
                if buf.is_empty() {
                    panic!("cannot call read with an empty buffer");
                }
                if self.read_buf.is_empty() {
                    let new_waker = cx.waker();
                    if let Some(ref mut waker) = self.read_waker {
                        if !new_waker.will_wake(waker) {
                            *waker = new_waker.clone();
                        }
                    } else {
                        self.read_waker = Some(new_waker.clone());
                    }
                    return Poll::Pending;
                }

                let mut idx = 0;
                while !self.read_buf.is_empty() && idx < buf.len() {
                    buf[idx] = self.read_buf.pop();
                    idx += 1;
                }

                Poll::Ready(idx)
            }

            /// "Turn" the reactor, emptying the write buffer and filling the
            /// read buffer.
            pub fn turn(&mut self) {
                let (ref mut tx, ref mut rx) = self.device;
                let sr = unsafe { &*$usart::ptr() }.sr.read();
                if sr.rxne().bit_is_set() {
                    if let Ok(byte) = rx.read() {
                        if self.read_buf.can_push() {
                            self.read_buf.push(byte);
                        }
                    }
                    if let Some(waker) = self.read_waker.take() {
                        waker.wake();
                    }
                }
                if sr.txe().bit_is_set() {
                    if !self.write_buf.is_empty() {
                        let byte = self.write_buf.pop();
                        tx.write(byte).unwrap();
                    } else {
                        unsafe { &*$usart::ptr() }
                            .cr1
                            .modify(|_, w| w.txeie().clear_bit());
                    }
                    if let Some(waker) = self.write_waker.take() {
                        waker.wake();
                    }
                }
                if sr.tc().bit_is_set() {
                    if self.write_buf.is_empty() {
                        if let Some(waker) = self.flush_waker.take() {
                            waker.wake();
                        }
                        unsafe { &*$usart::ptr() }
                            .cr1
                            .modify(|_, w| w.tcie().clear_bit());
                    }
                }
            }
        }

        impl<R, B> Write for Tx<R, $usart, B>
        where
            R: Mutex<T = Reactor<$usart, B>>,
            B: RealArray<u8> + AsRef<[u8]> + AsMut<[u8]>,
        {
            type Error = !;
            fn poll_write(
                self: Pin<&mut Self>,
                cx: &mut Context<'_>,
                buf: &[u8],
            ) -> Poll<Result<usize, !>> {
                let this = unsafe { self.get_unchecked_mut() };
                this.reactor
                    .lock(move |reactor| reactor.poll_write(cx, buf))
                    .map(Ok)
            }
            fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), !>> {
                let this = unsafe { self.get_unchecked_mut() };
                this.reactor
                    .lock(move |reactor| reactor.poll_flush(cx))
                    .map(Ok)
            }
            fn poll_close(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Result<(), !>> {
                Poll::Ready(Ok(()))
            }
        }

        impl<R, B> Read for Rx<R, $usart, B>
        where
            R: Mutex<T = Reactor<$usart, B>>,
            B: RealArray<u8> + AsRef<[u8]> + AsMut<[u8]>,
        {
            type Error = !;

            fn poll_read(
                self: Pin<&mut Self>,
                cx: &mut Context<'_>,
                buf: &mut [u8],
            ) -> Poll<Result<usize, !>> {
                let this = unsafe { self.get_unchecked_mut() };
                this.reactor
                    .lock(|reactor| reactor.poll_read(cx, buf))
                    .map(Ok)
            }
        }

        impl<R, B> ReactorExt<$usart, B> for R
        where
            R: Mutex<T = Reactor<$usart, B>>,
            B: RealArray<u8> + AsRef<[u8]> + AsMut<[u8]>,
        {
            fn take(&mut self) -> (Tx<&'_ mut Self, $usart, B>, Rx<&'_ mut Self, $usart, B>) {
                self.lock(|reactor| reactor.take());
                unsafe { &*$usart::ptr() }
                    .cr1
                    .modify(|_, w| w.rxneie().set_bit());
                // Safety note: Intentionally going around the borrow checker
                // here because the read/write methods operate on entirely
                // separate parts of the reactor structure, so this is
                // conceptually a "split borrow."
                let ptr = self as *mut Self;
                (
                    Tx {
                        reactor: unsafe { &mut *ptr },
                        _ph: PhantomData,
                    },
                    Rx {
                        reactor: unsafe { &mut *ptr },
                        _ph: PhantomData,
                    },
                )
            }
        }
    };
}

hal!(USART1);
hal!(USART2);
hal!(USART3);
