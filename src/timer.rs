use core::{
    marker::PhantomData,
    pin::Pin,
    task::{
        Context,
        Poll,
        Waker,
    },
};

use futures::prelude::*;

use embedded_hal::timer::{
    CountDown,
    Periodic,
};

use rtfm::Mutex;

#[cfg(feature = "medium")]
use crate::stm32::TIM4;

use crate::{
    hal::{
        time::Hertz,
        timer as hal,
    },
    stm32::{
        TIM1,
        TIM2,
        TIM3,
    },
};

/// Timer "Reactor"
///
/// Manages the timer device. Can be "turned" from an interrupt.
pub struct Reactor<TIMER> {
    owned: bool,
    has_elapsed: bool,
    device: hal::CountDownTimer<TIMER>,
    waker: Option<Waker>,
}

impl<TIMER> Reactor<TIMER> {
    /// Create a new reactor from the hardware timer
    pub fn new(device: hal::CountDownTimer<TIMER>) -> Self {
        Self {
            owned: false,
            has_elapsed: false,
            waker: None,
            device,
        }
    }

    unsafe fn steal(&mut self) -> bool {
        let preowned = self.owned;
        self.owned = true;
        !preowned
    }

    fn take(&mut self) {
        unsafe { assert!(self.steal()) }
    }

    unsafe fn release(&mut self) {
        self.owned = false;
    }
}

/// A timer backed by a [Reactor]
pub struct Timer<R, TIMER>
where
    R: Mutex<T = Reactor<TIMER>>,
{
    reactor: R,
    _ph: PhantomData<Reactor<TIMER>>,
}

impl<R, TIMER> Drop for Timer<R, TIMER>
where
    R: Mutex<T = Reactor<TIMER>>,
{
    fn drop(&mut self) {
        self.reactor.lock(|reactor| unsafe {
            reactor.release();
        });
    }
}

unsafe impl<R, TIMER> Send for Timer<R, TIMER> where R: Mutex<T = Reactor<TIMER>> + Send {}
unsafe impl<R, TIMER> Sync for Timer<R, TIMER> where R: Mutex<T = Reactor<TIMER>> + Sync {}

/// Extension trait for mutexes around a [Reactor]
pub trait ReactorExt<TIMER>: Mutex<T = Reactor<TIMER>> + Sized {
    /// "Take" the reactor, allowing it to be used as a timer.
    ///
    /// Should panic if called repeatedly without dropping the timer.
    fn take(&mut self) -> Timer<&'_ mut Self, TIMER>;
}

macro_rules! hal {
    ($timer:ident) => {
        impl Reactor<$timer> {
            fn poll(&mut self, cx: &mut Context<'_>) -> Poll<()> {
                if self.has_elapsed {
                    self.has_elapsed = false;
                    Poll::Ready(())
                } else {
                    let new_waker = cx.waker();
                    if let Some(ref mut waker) = self.waker {
                        if !new_waker.will_wake(waker) {
                            *waker = new_waker.clone();
                        }
                    } else {
                        self.waker = Some(new_waker.clone());
                    }
                    Poll::Pending
                }
            }

            /// "Turn" the timer reactor
            ///
            /// Checks if the timer has elapsed, clears the interrupt flag, and
            /// wakes any registered waker.
            pub fn turn(&mut self) {
                if let Ok(()) = self.device.wait() {
                    self.has_elapsed = true;
                    if let Some(waker) = self.waker.take() {
                        waker.wake();
                    }
                }
            }
        }

        impl<R> CountDown for Timer<R, $timer>
        where
            R: Mutex<T = Reactor<$timer>>,
        {
            type Time = Hertz;

            fn start<T>(&mut self, timeout: T)
            where
                T: Into<Hertz>,
            {
                self.reactor.lock(|reactor| {
                    reactor.device.start(timeout);
                });
            }
            fn wait(&mut self) -> nb::Result<(), void::Void> {
                let poll = self.reactor.lock(|reactor| {
                    reactor.poll(&mut Context::from_waker(&futures::task::noop_waker()))
                });
                match poll {
                    Poll::Ready(()) => Ok(()),
                    Poll::Pending => Err(nb::Error::WouldBlock),
                }
            }
        }

        impl<R> Periodic for Timer<R, $timer> where R: Mutex<T = Reactor<$timer>> {}

        impl<R> Stream for Timer<R, $timer>
        where
            R: Mutex<T = Reactor<$timer>>,
        {
            type Item = ();
            fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<()>> {
                let this = unsafe { self.get_unchecked_mut() };
                this.reactor.lock(|reactor| reactor.poll(cx)).map(Some)
            }
        }

        impl<R> ReactorExt<$timer> for R
        where
            R: Mutex<T = Reactor<$timer>>,
        {
            fn take(&mut self) -> Timer<&'_ mut Self, $timer> {
                self.lock(|reactor| {
                    reactor.device.listen(hal::Event::Update);
                    reactor.take();
                });

                Timer {
                    reactor: self,
                    _ph: PhantomData,
                }
            }
        }
    };
}

hal!(TIM1);
hal!(TIM2);
hal!(TIM3);

#[cfg(feature = "medium")]
hal!(TIM4);
